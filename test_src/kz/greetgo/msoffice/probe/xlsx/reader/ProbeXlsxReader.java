package kz.greetgo.msoffice.probe.xlsx.reader;

import kz.greetgo.msoffice.xlsx.reader.Sheet;
import kz.greetgo.msoffice.xlsx.reader.XlsxReader;

import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ProbeXlsxReader {
  public static void main(String[] args) throws Exception {
    System.out.println("2K3oAhsK3W");

    Path dir = Paths.get("/home/pompei/Downloads");
    Path xlsxFile = dir.resolve("Новая таблица (17).xlsx");

    try (FileInputStream fileInputStream = new FileInputStream(xlsxFile.toFile())) {

      try (XlsxReader xlsxReader = new XlsxReader()) {

        xlsxReader.read(fileInputStream);

        Sheet sheet = xlsxReader.tabSelectedSheet();

        String text = sheet.cell(10, 0).asText();
        System.out.println("27kTe6FYuK :: text = " + text);

      }

    }
  }
}
